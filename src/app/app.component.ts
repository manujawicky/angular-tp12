import {Component} from '@angular/core';
import {Hero} from './model/hero';
import {heros} from './heros';
import {HerosService} from './heros.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'heros!!!';
  width = 100;
  drawerOpened = true;


  onDrawerClick() {
    this.drawerOpened = !this.drawerOpened;
  }
}
