import {Injectable} from '@angular/core';
import {Hero} from './model/hero';
import {heros} from './heros';
import {HttpClient} from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class HerosService {

  constructor(private httpClient: HttpClient, private fs: AngularFirestore) {
  }

  getHeros(): Observable<Hero[]> {

    return this.fs.collection<Hero>('1234').valueChanges();

    /*const herosObservable: Subject<Hero[]> = new Subject<Hero[]>();

    this.httpClient.get('https://gateway.marvel.com/v1/public' +
      '/comics?limit=99&apikey=1875197149b4ba405c9bd68d0e55' +
      'bfc3&hash=fb8d3fc88958a6ee8001cbe23030a0d0')
      .subscribe((value: any) => {
        herosObservable.next(value.data.results);
      });
    return herosObservable.asObservable();*/
  }

}
