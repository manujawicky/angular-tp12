import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {HerosModule} from './heros/heros.module';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'heros', loadChildren: () => import('./heros/heros.module').then(m => m.HerosModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
