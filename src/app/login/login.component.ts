import {Component, OnInit} from '@angular/core';
import {LoginService} from '../login.service';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm;


  constructor(private loginService: LoginService, private formBuilder: FormBuilder, private router: Router) {

    this.loginForm = formBuilder.group ({
    emailField: [' ', [Validators.required, Validators.email]],
    passwordField: [' ', [Validators.minLength(5), Validators.required]]


  });
}

  ngOnInit() {
  }

  onLogin() {
      this.loginService.authenticate(this.loginForm.value.emailField , this.loginForm.value.passwordField).then(value => {
      this.router.navigateByUrl('heros');
    }, error => {
      alert('Error login:' + error);
    });

  }
}
