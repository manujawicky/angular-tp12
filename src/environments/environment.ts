// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAQv0DsDY_SSXuHm5vCSuZQHu8OxUOpFcs",
    authDomain: "angulartp-fe7d2.firebaseapp.com",
    databaseURL: "https://angulartp-fe7d2.firebaseio.com",
    projectId: "angulartp-fe7d2",
    storageBucket: "angulartp-fe7d2.appspot.com",
    messagingSenderId: "291928662461",
    appId: "1:291928662461:web:1eb1daccc7df51dc0872d4",
    measurementId: "G-0H5G2P0FP4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
