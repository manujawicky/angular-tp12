import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Hero} from '../model/hero';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit, OnDestroy {
  @Input() hero: Hero;

  constructor() {
  }

  ngOnInit() {
  }

  onHeroClick(clickedHero: Hero) {
    console.log(clickedHero);
  }

  ngOnDestroy(): void {

  }

}
