import {Injectable} from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private angularFireAuth: AngularFireAuth) {

  }

  authenticate(userName: string, password: string): Promise<any> {
    return this.angularFireAuth.auth.signInWithEmailAndPassword(userName, password);

  }
}
