import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[appHilight]'
})
export class HilightDirective {

  constructor(el: ElementRef) {
    el.nativeElement.style.background = 'Green';
  }
}
