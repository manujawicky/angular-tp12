import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Hero} from '../model/hero';
import {HerosService} from '../heros.service';

@Component({
  selector: 'app-heros',
  templateUrl: './heros.component.html',
  styleUrls: ['./heros.component.scss']
})
export class HerosComponent implements OnInit {

  appComponentHero: Observable<Hero[]>;

  constructor(private herosService: HerosService) {
    this.appComponentHero = herosService.getHeros();
  }

  ngOnInit() {
  }

}
