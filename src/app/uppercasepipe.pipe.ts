import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'uppercasepipe'
})
export class UppercasepipePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value == null) {
      return 'Empty';
    } else {
      return 'UpperCased:' + value.toString().toUpperCase();
    }

  }

}
