import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HerosRoutingModule } from './heros-routing.module';
import {HerosComponent} from './heros.component';
import {HeroComponent} from '../hero/hero.component';
import {HerosService} from '../heros.service';
import {UppercasepipePipe} from '../uppercasepipe.pipe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCardModule, MatIconModule, MatListModule, MatSidenavModule, MatToolbarModule} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';


@NgModule({
  declarations: [  HerosComponent, HeroComponent, UppercasepipePipe],
  imports: [
    CommonModule,
    HerosRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    HttpClientModule,
    AngularFirestoreModule
  ],
  providers: [HerosService]
})
export class HerosModule { }
